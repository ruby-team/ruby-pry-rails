require 'gem2deb/rake/testtask'

task :test => :console do
  Gem2Deb::Rake::TestTask.new do |t|
    t.libs.concat %w(pry-rails spec)
    t.pattern = './spec/**/*_spec.rb'
  end
end

desc 'Start the Rails console'
task :console => :development_env do
  if (Rails::VERSION::MAJOR == 5 && Rails::VERSION::MINOR >= 1) ||
      Rails::VERSION::MAJOR >= 6
    require 'rails/command'
    require 'rails/commands/console/console_command'
  else
    require 'rails/commands/console'
  end

  Rails::Console.start(Rails.application)
end

task :development_env do
  ENV['RAILS_ENV'] = 'development'
  require File.expand_path('../../spec/config/environment', __FILE__)
  Dir.chdir(Rails.application.root)
end

task :default => :test

